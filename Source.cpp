#include <iostream>
#include <iomanip>
using namespace std;

void inputAndCheck(float&) //inputs must be > 0 (for floats)
void inputAndCheck(int&); //inputs must be > 0 (for integers)
float bookSurfaceArea(float, float, float); //Calculate surface area of a book
float inchToCenti(float); //Convert inches to centimeters
float poundToGrams(float); //Convert pounds to grams
int stockCheck(int); //Check age of a book and calculate discount
float pricing(float, float, int); //calculate total price
int member(int); //check customers if they're members
float saleMemberPrice(float, float); //calculate discount 20% of cheapest book
float cheapestBookFind(int, int, float,float); //find out the cheapest book for members

int main() {
	int quantity = 0, memberStatus = 2;
	float totalPrice = 0;

	//print head of bill
	cout << endl << "            K E N ' S            " << endl;
	cout << "       B O O K S  S T O R E      " << endl;
	cout << endl << "================================" << endl << endl;

	//check member?
	memberStatus = member(memberStatus);

	//how many books they bought?
	cout << "How many books? : ";
	inputAndCheck(quantity);

	//int* array = new int[len];
	//float bookPrice[quantity] = {};
	float bookPrice, cheapest = 0;

	for (int i = 0; i < quantity; i++) { //loop for input various book

		cout << "------------ BOOK #" << i + 1 << " -----------" << endl;

		float height, width, length, weight;
		int stock = 1, discount;

		//input details of a book
		cout << "Book height   (inch) : ";
		inputAndCheck(height);
		cout << "Book width    (inch) : ";
		inputAndCheck(width);
		cout << "Book length   (inch) : ";
		inputAndCheck(length);
		cout << "Book weight (pounds) : ";
		inputAndCheck(weight);
		cout << "Book age       (day) : ";
		inputAndCheck(stock);



		//calculate price
		bookPrice = pricing(bookSurfaceArea(height, width, length), poundToGrams(weight), stock);
		totalPrice = totalPrice + bookPrice;
		cheapest = cheapestBookFind(memberStatus, quantity, bookPrice,cheapest);

		}

	//print an end of bill
	cout << "--------------------------------" << endl;
	cout << "Subtotal       : " << setw(10) << totalPrice << " baht" << endl;
	cout << "Member discount: " << setw(10) << cheapest * 20 / 100 << " baht" << endl;
	
	cout << "Total Price    : " << setw(10) << totalPrice - cheapest * 20 / 100 << " baht" << endl;
	cout << endl << "=================================" << endl << endl;
	cout << "         T H A N K  Y O U        " << endl;
	cout << "   H A V E  A  N I C E  D A Y !  " << endl;
	
}
void inputAndCheck(int& input) {
	int check = 0;
	while (check <= 0) {
		cin >> check;

		if (check <= 0) {
			cout << "Invalid input, enter again : ";
		}
		else if (check == 0) {
			cout << "Input must be > 0 : ";
		}
		else {
			input = check;
			return;
		}

	}
}
void inputAndCheck(float& input) {
	float check = 0;
	while (check <= 0) {
		cin >> check;

		if (check <= 0) {
			cout << "Invalid input, enter again : ";
		}  else if(check == 0) {
			cout << "Input must be > 0 : ";
		}
		else {
			input = check;
			return;
		}
		
	}
}

float inchToCenti(float x) {
	return x*2.54;
}

float bookSurfaceArea(float height, float width, float length) {
	//Convert to centimeters
	height = inchToCenti(height);
	width = inchToCenti(width);
	length = inchToCenti(length);

	float bookArea = 2 * (height*width) + 2 * (width*length) + 2 * (height*length);
	return bookArea;
}

float poundToGrams(float x) {
	return x*0.00220462262;
}

int stockCheck(int age) {
	int cost;

	if (age > 30) {
		cost = 20 + (2 * (age - 30)); //Add 20 because have to add discout frome day 10 to 30 (1 bath per day) 
	}
	else if (age > 10) {
		cost = age - 10;
	}

	return cost; 
}

float pricing(float area, float weight, int age) {
	float price = area*weight * 10;
	if (age > 10) {
		float discout = stockCheck(age);
		cout << "Price          : " << setw(10) << price << " baht" << endl;
		cout << "Dicount        : " << setw(10) << discout << " baht" << endl;
		price = price - discout;
		if (price < 0){
			price = 0; //Price can't be lower than 0 ->> it's had to be for FREE
		}
		cout << "                 " << setw(10) << price << " baht" << endl << endl;
	}
	else {
		cout << "Price          : " << setw(10) << price << " baht" << endl << endl;
	}
	return price;
}

int member(int memberCheck) {

	while (memberCheck != 0 && memberCheck != 1) {
		cout << "Member? (1 = yes, 0 = no) : ";
		cin >> memberCheck;
		if (memberCheck != 0 && memberCheck != 1) { //inout must be 0 and 1
			cout << "Invalid input, please enter new input." << endl; 
		}
		else if (memberCheck == 1) {
			cout << "   W E L C O M E  M E M B E R !  " << endl; //Check if customers are member 
		}
	}

	return memberCheck;
}

float memberPrice(float totalPrice, float cheapestBook) {
	cheapestBook * 20 / 100; //Culculate the cheapest 20% discout
	return totalPrice;
}


float cheapestBookFind(int memberStatus, int bookQuantity, float bookPrice, float cheapest) {
	if (memberStatus == 1) {

		if (bookQuantity >= 3) {

			//cout << "User buy more than 3 books" << endl; //Check loops if user buy more 3 books

			for (int j = 0; j < bookQuantity; j++) {

				if (cheapest > bookPrice || cheapest == 0) {
					cheapest = bookPrice;

				}
			}
			//cout << "now cheapest " << cheapest << endl; //Check current cheapest book in each input
		}
	}
	return cheapest;
}
